//
//  ViewController.m
//  PDFViewer
//
//  Created by Reinhold on 27/07/15.
//  Copyright (c) 2015 talentify. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) IBOutlet UIDocumentInteractionController *documentInteractionController;

@end

@implementation ViewController 

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.webView.delegate = self;
    
    NSString *urlAddress = @"http://www.flowio.at/projects/pdfViewer/index.html";
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    
    
    
}


#pragma - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

    NSURL *url = request.URL;
    
    NSLog(@"%@",url.absoluteString);
    
    if ([url.absoluteString hasSuffix:@"pdf"]) {
        NSLog(@"handle link to pdf");
        
        //download pdf first - UIDocumentInteractionController works only with lokal files
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:nil delegateQueue:nil];
        
        NSURLSessionDownloadTask *getPDFTask =
        [session downloadTaskWithURL:url
         
                   completionHandler:^(NSURL *location,
                                       NSURLResponse *response,
                                       NSError *error) {
                       if (!error) {
                           NSLog(@"finished downloading pdf to %@", location.absoluteString);
                           
                           
                           NSError * err = NULL;
                           NSString * path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"temp.pdf"];
                           BOOL success = [[NSData dataWithContentsOfURL:location] writeToFile:path
                                                        options:NSDataWritingFileProtectionComplete
                                                          error:&err];
                           
                           if (success) {
                               
                               // Initialize Document Interaction Controller
                               self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"file://%@",path]]];
                               
                               // Configure Document Interaction Controller
                               [self.documentInteractionController setDelegate:self];
                               
                               // Preview PDF
                               [self.documentInteractionController presentPreviewAnimated:YES];
                           
                           }
                           else {
                               NSLog(@"Error: %@", err.localizedDescription);
                           }

                       }
                       
                   }];
        
        [getPDFTask resume];
        
        return NO;
        
    }
    else {
        return YES;
    }
    
    
}

#pragma - UIDocumentInteractionControllerDelegate

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
