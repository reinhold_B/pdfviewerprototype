//
//  ViewController.h
//  PDFViewer
//
//  Created by Reinhold on 27/07/15.
//  Copyright (c) 2015 talentify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate, UIDocumentInteractionControllerDelegate>


@end

